# Portal 2 BSC addon (Better SChinese)

----

### 本仓库仅用作分发渠道

![封面](./cover.png)

觉得官方的“机翻”简中太拉跨？来试试“传送门2 更好的简中”插件吧。

从零开始的台词译本！贴图内容汉化（可选/未完成）！以及更高清的纹理！

.
.
.

```
系统广播: 炮塔回收线路不是交通工具，请离开炮塔回收线路。
```

```
Wheatley: 这是神经毒素发生器。比我想象的还要大，没办法轻易推倒它。得想个办法。
```

```
GLaDOS: 下个测试涉及折射方块。我刚好在你开启你人生中新“篇章”的时候建好它们。所以让我们看看它们怎样。
```

```
GLaDOS: 哦，嗨。所以，你还好吧？ 因为我是颗土豆。[鼓掌鼓掌鼓掌]
```

```
Cave Johnson: 顺便说一下，所有这些浓缩球都是由石棉制成的，可以防老鼠。如果你感到呼吸困难、持续性干咳或心脏停止跳动，请告知我们。因为这不是测试的一部分，而是石棉造成的。
```
.
.
.
